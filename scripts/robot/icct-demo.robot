*** Settings ***
Documentation          Test ICC transport between root-cell and freertos-icct-demo inmate.
Library                SSHLibrary
Suite Setup            System Setup
Suite Teardown         System Teardown

*** Variables ***
${TATE-HOST}           tate1
${TATE-USER}           tateusr
${TATE-PWD}            tatepw
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-KEY}             ../jenkins/jenkins-key
${JAILHOUSE_DIR}       /usr/share/jailhouse
${CELL_CONFIG}	       freertos-icct-demo.cell
${INMATE}	       freertos-icct-demo.bin

*** Test Cases ***
Pump string to icct in root-cell
     Switch Connection	  bpi
     ${stdout}    ${rc}=   Execute Command    /opt/icc/icctpump    return_stdout=True   return_rc=True
     Log    ${stdout}
     Should Be Equal As Integers    ${rc}   0
     Should Contain   ${stdout}    recv() ('Hello from icctpump') from ufds[0]

*** Keywords ***
System Setup
	Open TATE Connection And Log In
	Switch Bananapi Off and On
	Start terminal for freertos-icct-demo on tate
	Log In to Bananapi
	Wait for Prompt
	Load Jailhouse Module
	Start Jailhouse Root Cell
	Create FreeRTOS cell
	Load FreeRTOS inmate
	Start FreeRTOS inmate
	Check output from FreeRTOS inmate

Open TATE Connection And Log In
     Set Default Configuration   timeout=10s    prompt=$
     Open Connection    ${TATE-HOST}    alias=tate
     Login              ${TATE-USER}    ${TATE-PWD}

Switch Bananapi Off and On
       Execute Command    ./tatectl.sh -f 2
       Sleep              1s           Give the relay a break 
       Execute Command    ./tatectl.sh -n 2

Start terminal for freertos-icct-demo on tate
# We need to do this here before the demo starts to make sure we do not receive
# garbage as a first character - the Read command further down can't handle that
      Write     picocom /dev/ttyTGT9 -b 115200
      Read Until        Terminal ready


Log In to Bananapi
       Wait Until Keyword Succeeds  2 min   50 sec   Open Bananapi Connection And Log In

Open Bananapi Connection And Log In
     Set Default Configuration   timeout=10s    prompt=#
     Open Connection    ${BPI-HOST}     alias=bpi
     Login With Public Key   ${BPI-USER}   ${BPI-KEY}

Wait for Prompt
     Write      ls
     Read Until Prompt

Load Jailhouse Module
     ${rc}=  Execute Command   modprobe jailhouse     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start Jailhouse Root Cell
     ${rc}=  Execute Command   jailhouse enable ${JAILHOUSE_DIR}/cells/plamics-bpi-root.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0
     Start Command   /opt/icc/icctd

Create FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell create ${JAILHOUSE_DIR}/cells/${CELL_CONFIG}     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Load FreeRTOS inmate
     ${rc}=  Execute Command   jailhouse cell load FreeRTOS ${JAILHOUSE_DIR}/inmates/${INMATE}     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start FreeRTOS inmate
     ${rc}=  Execute Command   jailhouse cell start FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Check output from FreeRTOS inmate
     Switch Connection         tate
     ${output}=         Read Until     vTaskStartScheduler goes active
     Log          ${output}

System Teardown
       Shutdown FreeRTOS cell
       Destroy FreeRTOS cell
       Disable Jailhouse
       Close All Connections

Shutdown FreeRTOS cell
     Switch Connection         bpi
     ${rc}=  Execute Command   jailhouse cell shutdown FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Destroy FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell destroy FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Disable Jailhouse
     ${rc}=  Execute Command   jailhouse disable     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

