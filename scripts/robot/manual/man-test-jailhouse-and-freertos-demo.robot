*** Settings ***
Documentation          Test Jailhouse with uart-demo.
Library                SSHLibrary
Suite Teardown         Close All Connections

*** Variables ***
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-KEY}             ../../jenkins/jenkins-key
${JAILHOUSE_DIR}       /usr/share/jailhouse


*** Test Cases ***
Log In to Bananapi
       Wait Until Keyword Succeeds  4 sec   2 sec   Open Bananapi Connection And Log In

Wait for Prompt
     Write      ls
     Read Until Prompt

Load Jailhouse Module
     ${rc}=  Execute Command   modprobe jailhouse     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start Jailhouse Root Cell
     ${rc}=  Execute Command   jailhouse enable ${JAILHOUSE_DIR}/cells/plamics-bpi-root.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0
     
Create FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell create ${JAILHOUSE_DIR}/cells/freertos-cell.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Load FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell load FreeRTOS ${JAILHOUSE_DIR}/inmates/freertos-demo.bin     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell start FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Shutdown FreeRTOS cell
     Switch Connection         bpi
     ${rc}=  Execute Command   jailhouse cell shutdown FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Destroy FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell destroy FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Disable Jailhouse
     ${rc}=  Execute Command   jailhouse disable     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

*** Keywords ***
Open Bananapi Connection And Log In
     Set Default Configuration   timeout=10s    prompt=#
     Open Connection    ${BPI-HOST}     alias=bpi
     Login With Public Key   ${BPI-USER}   ${BPI-KEY}
