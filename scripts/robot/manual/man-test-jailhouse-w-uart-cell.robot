*** Settings ***
Documentation          Test Jailhouse with uart-demo.
Library                SSHLibrary
Suite Teardown         Close All Connections

*** Variables ***
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-KEY}             ../../jenkins/jenkins-key
${JAILHOUSE_DIR}       /usr/share/jailhouse

*** Test Cases ***
Log In to Bananapi
       Wait Until Keyword Succeeds  10 sec   3 sec   Open Bananapi Connection And Log In

Wait for Prompt
     Write      ls
     Read Until Prompt

Load Jailhouse Module
     Execute Command   depmod
     ${rc}=  Execute Command   modprobe jailhouse     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start Jailhouse Root Cell
     ${rc}=  Execute Command   jailhouse enable ${JAILHOUSE_DIR}/cells/bananapi.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0
     
Create uart-demo cell
     ${rc}=  Execute Command   jailhouse cell create ${JAILHOUSE_DIR}/cells/bananapi-uart-demo.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Load uart-demo
     ${rc}=  Execute Command   jailhouse cell load bananapi-uart-demo ${JAILHOUSE_DIR}/inmates/uart-demo.bin     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start uart-demo cell
     ${rc}=  Execute Command   jailhouse cell start bananapi-uart-demo    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

#Check output from uart-demo
#     Switch Connection         tate
#     ${output}=         Read Until     Hello 535 from cell!
#     Log          ${output}    
      
*** Keywords ***
Open Bananapi Connection And Log In
     Set Default Configuration   timeout=10s    prompt=#
     Open Connection    ${BPI-HOST}     alias=bpi
     Login With Public Key   ${BPI-USER}   ${BPI-KEY}
