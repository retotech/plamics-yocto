*** Settings ***
Documentation          Test Jailhouse ivshmem-demo.
Library                SSHLibrary
Suite Setup            Open TATE Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${TATE-HOST}           tate1
${TATE-USER}           tateusr
${TATE-PWD}            tatepw
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-KEY}             ../jenkins/jenkins-key
${JAILHOUSE_DIR}       /usr/share/jailhouse
${CELL_CFG_DIR}	       ${JAILHOUSE_DIR}/cells
${ROOT_CELL_CFG}       ${CELL_CFG_DIR}/plamics-bpi-root.cell
${ROOT_CELL_HELLO_STR}  'Hello from root cell'
${CELL_CFG}	       ${CELL_CFG_DIR}/freertos-ivshmem-demo.cell
${CELL_NAME}	       FreeRTOS
${INMATE_DIR}	       ${JAILHOUSE_DIR}/inmates
${INMATE}	       ${INMATE_DIR}/ivshmem-demo.bin
${INMATE_HELLO_STR}    'Hello from bare-metbl ivshmem-demo inmate!!!'

*** Test Cases ***
Switch Bananapi Off and On
       Execute Command    ./tatectl.sh -f 2
       Sleep              1s           Give the relay a break 
       Execute Command    ./tatectl.sh -n 2

Start terminal for ivshmem-demo on tate
# We need to do this here before the demo starts to make sure we do not receive
# garbage as a first character - the Read command further down can't handle that
      Write     picocom /dev/ttyTGT9 -b 115200
      Read Until        Terminal ready

Log In to Bananapi
       Wait Until Keyword Succeeds  2 min   5 sec   Open Bananapi Connection And Log In

Wait for Prompt
     Write      ls
     Read Until Prompt

Load Jailhouse Module
     ${rc}=  Execute Command   modprobe jailhouse     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start Jailhouse Root Cell
     ${rc}=  Execute Command   jailhouse enable ${ROOT_CELL_CFG}     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Write root cell string to shmem in root cell
     ${rc}=  Execute Command   /opt/ivshmem-utils/shmem_pump /dev/uio0 ${ROOT_CELL_HELLO_STR}  return_stdout=False  return_rc=True
     Should Be Equal As Integers    ${rc}   0

Read root cell string from shmem in root cell
     ${stdout}  ${rc}=  Execute Command   /opt/ivshmem-utils/shmem_dump /dev/uio0  return_stdout=True  return_rc=True
     Should Be Equal As Integers    ${rc}   0
     Log    ${stdout}
     Should Contain  ${stdout}  Hello from root cell

Create bare-metal cell
     ${stdout}  ${rc}=  Execute Command   jailhouse cell create ${CELL_CFG}     return_stdout=True     return_rc=True
     Log  ${stdout}
     Should Be Equal As Integers    ${rc}   0

Load bare-metal inmate into cell
     ${stdout}  ${rc}=  Execute Command   jailhouse cell load ${CELL_NAME} ${INMATE} -s "pci-cfg-base=0x02000000 ivshmem-irq=155" -a 0x100     return_stdout=True     return_rc=True
     Log  ${stdout}
     Should Be Equal As Integers    ${rc}   0

Verify no ivshmem interrupts in root-cell
     ${stdout}  ${rc}=  Execute Command  grep ivshmem /proc/interrupts | awk '{print $2}'  return_stdout=True  return_rc=True
     Should Be Equal As Integers  ${rc}  0
     Log  ${stdout}
     Should Contain  ${stdout}  0

Start bare-metal cell
     ${stdout}  ${rc}=  Execute Command   jailhouse cell start ${CELL_NAME}    return_stdout=True     return_rc=True
     Log  ${stdout}
     Should Be Equal As Integers    ${rc}   0

Check output from bare-metal inmate
     Switch Connection         tate
     ${output}=         Read Until     Hello from root cell
     Log          ${output}

Switch back to root cell
     Switch Connection         bpi
     Sleep   5s   Wait for bare-metal ivshmem-demo inmate to start
     Write   clear
     Read Until Prompt

Verify ivshmem interrupts in root-cell
     ${stdout}  ${rc}=  Execute Command  grep ivshmem /proc/interrupts | awk '{print $2}'  return_stdout=True  return_rc=True
     Should Be Equal As Integers  ${rc}  0
     Log  ${stdout}
     Should Contain  ${stdout}  1

Read bare-metal str from shmem in root cell
     ${stdout}  ${rc}=  Execute Command   /opt/ivshmem-utils/shmem_dump /dev/uio0   return_stdout=True   return_rc=True
     Log   ${stdout}
     Should Be Equal As Integers    ${rc}   0
     Should Contain  ${stdout}  Hello from bare-metal ivshmem-demo inmate!!!

Send ivshmem interrupts from root-cell
     ${stdout}  ${rc}=  Execute Command  /opt/ivshmem-utils/uio_send /dev/uio0 10 0 0  return_stdout=True  return_rc=True
     Log  ${stdout}
     Should Be Equal As Integers  ${rc}  0
     Should Contain  ${stdout}  [UIO] Exiting...

Verify ivshmem interrupts handled in bare-metal inmate
     Switch Connection  tate
     ${stdout}=  Read Until  IVSHMEM: handle_irq(irqn:155) - interrupt #9
     Log  ${stdout}

Shutdown bare-metal cell
     Switch Connection  bpi
     ${stdout}  ${rc}=  Execute Command   jailhouse cell shutdown ${CELL_NAME}    return_stdout=True     return_rc=True
     Log   ${stdout}
     Should Be Equal As Integers    ${rc}   0

Destroy bare-metal cell
     ${stdout}  ${rc}=  Execute Command   jailhouse cell destroy ${CELL_NAME}    return_stdout=True     return_rc=True
     Log   ${stdout}
     Should Be Equal As Integers    ${rc}   0

Disable Jailhouse
     ${stdout}  ${rc}=  Execute Command   jailhouse disable     return_stdout=True     return_rc=True
     Log   ${stdout}
     Should Be Equal As Integers    ${rc}   0

*** Keywords ***
Open TATE Connection And Log In
     Set Default Configuration   timeout=10s    prompt=$
     Open Connection    ${TATE-HOST}    alias=tate
     Login              ${TATE-USER}    ${TATE-PWD}

Open Bananapi Connection And Log In
     Set Default Configuration   timeout=10s    prompt=#
     Open Connection    ${BPI-HOST}     alias=bpi
     Login With Public Key   ${BPI-USER}   ${BPI-KEY}
