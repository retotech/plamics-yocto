*** Settings ***
Documentation          Test Jailhouse ivshmem-demo.
Library                SSHLibrary
Suite Setup            Open TATE Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${TATE-HOST}           tate1
${TATE-USER}           tateusr
${TATE-PWD}            tatepw
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-KEY}             ../jenkins/jenkins-key
${JAILHOUSE_DIR}       /usr/share/jailhouse
${CELL_CONFIG}	       freertos-ivshmem-demo.cell
${INMATE}	       freertos-ivshmem-demo.bin

*** Test Cases ***
Switch Bananapi Off and On
       Execute Command    ./tatectl.sh -f 2
       Sleep              1s           Give the relay a break 
       Execute Command    ./tatectl.sh -n 2

Start terminal for ivshmem-demo on tate
# We need to do this here before the demo starts to make sure we do not receive
# garbage as a first character - the Read command further down can't handle that
      Write     picocom /dev/ttyTGT9 -b 115200
      Read Until        Terminal ready

Log In to Bananapi
       Wait Until Keyword Succeeds  2 min   5 sec   Open Bananapi Connection And Log In

Wait for Prompt
     Write      ls
     Read Until Prompt

Load Jailhouse Module
     ${rc}=   Execute Command   modprobe jailhouse     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Verify uio_ivshmem exists
     ${rc}=   Execute Command    [ -f /lib/modules/$(uname -r)/extra/uio_ivshmem.ko ]    return_stdout=False    return_rc=True
     Should Be Equal As Integers    ${rc}   0

Verify uio_ivshmem not yet insert
     ${rc}=  Execute Command    lsmod | grep uio_ivshmem    return_stdout=False    return_rc=True
     Should Be Equal As Integers    ${rc}   1

Start Jailhouse Root Cell
     ${rc}=  Execute Command   jailhouse enable ${JAILHOUSE_DIR}/cells/plamics-bpi-root.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Verify uio_ivshmem insert
     ${rc}=  Execute Command    lsmod | grep uio_ivshmem    return_stdout=False    return_rc=True
     Should Be Equal As Integers    ${rc}   0

Disable Jailhouse
     ${rc}=  Execute Command   jailhouse disable     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

*** Keywords ***
Open TATE Connection And Log In
     Set Default Configuration   timeout=10s    prompt=$
     Open Connection    ${TATE-HOST}    alias=tate
     Login              ${TATE-USER}    ${TATE-PWD}

Open Bananapi Connection And Log In
     Set Default Configuration   timeout=10s    prompt=#
     Open Connection    ${BPI-HOST}     alias=bpi
     Login With Public Key   ${BPI-USER}   ${BPI-KEY}
