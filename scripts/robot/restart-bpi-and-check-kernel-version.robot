*** Settings ***
Documentation          Restart Bananapi and try to log in. Check kernel version.
...                    Use a prepared boot script for u-boot to control the
...                    boot settings. This script needs to be placed under
...                    /export/tftpboot/tate/ before boot, typically by Jenkins.
Library                SSHLibrary
Suite Setup            Open TATE Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${TATE-HOST}           tate1
${TATE-USER}           tateusr
${TATE-PWD}            tatepw
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-PWD}             pi

*** Test Cases ***
Switch Bananapi Off and On
       Execute Command    ./tatectl.sh -f 2
       Sleep              1s           Give the relay a break 
       Execute Command    ./tatectl.sh -n 2

Log In to Bananapi
       Wait Until Keyword Succeeds  2 min   5 sec   Open Bananapi Connection And Log In

Test Uname
       ${output}=    Execute Command    uname -r
       Should Be Equal    ${output}    4.3.3-dirty


*** Keywords ***
Open TATE Connection And Log In
     Open Connection    ${TATE-HOST}
     Login              ${TATE-USER}    ${TATE-PWD}

Open Bananapi Connection And Log In
     Open Connection    ${BPI-HOST}
     Login              ${BPI-USER}     ${BPI-PWD}
