*** Settings ***
Documentation          Test Jailhouse ivshmem-demo.
Library                SSHLibrary
Suite Setup            Open TATE Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${TATE-HOST}           tate1
${TATE-USER}           tateusr
${TATE-PWD}            tatepw
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-KEY}             ../jenkins/jenkins-key
${JAILHOUSE_DIR}       /usr/share/jailhouse
${CELL_CONFIG}	       freertos-ivshmem-demo.cell
${INMATE}	       freertos-ivshmem-demo.bin

*** Test Cases ***
Switch Bananapi Off and On
       Execute Command    ./tatectl.sh -f 2
       Sleep              1s           Give the relay a break 
       Execute Command    ./tatectl.sh -n 2

Start terminal for ivshmem-demo on tate
# We need to do this here before the demo starts to make sure we do not receive
# garbage as a first character - the Read command further down can't handle that
      Write     picocom /dev/ttyTGT9 -b 115200
      Read Until        Terminal ready

Log In to Bananapi
       Wait Until Keyword Succeeds  2 min   5 sec   Open Bananapi Connection And Log In

Wait for Prompt
     Write      ls
     Read Until Prompt

Load Jailhouse Module
     ${rc}=  Execute Command   modprobe jailhouse     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start Jailhouse Root Cell
     ${rc}=  Execute Command   jailhouse enable ${JAILHOUSE_DIR}/cells/plamics-bpi-root.cell     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0
     
Create FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell create ${JAILHOUSE_DIR}/cells/${CELL_CONFIG}     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Load FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell load FreeRTOS ${JAILHOUSE_DIR}/inmates/${INMATE}     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Start FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell start FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Check output from FreeRTOS demo
     Switch Connection         tate
     ${output}=         Read Until     vTaskStartScheduler goes active
     Log          ${output}

Pump string to shmem in root-cell
     Switch Connection	  bpi
     ${rc}=   Execute Command    /opt/ivshmem-utils/shmem_pump /dev/uio0 "Hello from root-cell"    return_stdout=False   return_rc=True
     Should Be Equal As Integers    ${rc}   0

Wait until FreeRTOS command server ready
     Switch Connection    tate
     Set Client Configuration   newline=CRLF
     ${stdout}=   Read Until    Type help to veiw a list of registered commands
     Log      ${stdout}

#Run help command
#     Set Client Configuration   newline=CRLF
#     Write    help
#     ${stdout}=   Read Until    pump contents of memory\r\n
#     Log      ${stdout}

#Run ivshmem command
#     Set Client Configuration   newline=CRLF
#     Write    ivshmem
#     ${stdout}=   Read Until    irq:155
#     Log      ${stdout}

#Run mempump command
#     Write    mempump 0x7bf00000 Hej
#     ${stdout}=   Read Until    Hej
#     Log      ${stdout}

#Run memdump command
#     Write    memdump 0x7bf00000 0x10
#     ${stdout}=   Read Until    Hej
#     Log      ${stdout}

#Dump string from shmem in inmate
#     Write    memdump 0x7bf00000 0x10
#     ${stdout}=   Read Until    |Hello from root-|\r\n
#     Log      ${stdout}

Pump string to shmem in inmate
     Write    mempump 0x7bf00000 Hello_from_freertos_ivshmem-demo_inmate
     ${stdout}=   Read Until    \r\n

#Dump pumped string from shmem in inmate
#     Sleep    1s
#     Write    memdump 0x7bf00000 0x10\r\n
#     ${stdout}=   Read Until    Hello_from_free
#     Log      ${stdout}

Dump string from shmem in root-cell
     Switch Connection	  bpi
     ${stdout}   ${rc}=   Execute Command    /opt/ivshmem-utils/shmem_dump /dev/uio0   return_stdout=True   return_rc=True
     Should Be Equal As Integers    ${rc}   0
     Should Contain   ${stdout}    Hello_from_freertos_ivshmem-demo_inmate

Shutdown FreeRTOS cell
     Switch Connection         bpi
     ${rc}=  Execute Command   jailhouse cell shutdown FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Destroy FreeRTOS cell
     ${rc}=  Execute Command   jailhouse cell destroy FreeRTOS    return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

Disable Jailhouse
     ${rc}=  Execute Command   jailhouse disable     return_stdout=False     return_rc=True
     Should Be Equal As Integers    ${rc}   0

*** Keywords ***
Open TATE Connection And Log In
     Set Default Configuration   timeout=10s    prompt=$
     Open Connection    ${TATE-HOST}    alias=tate
     Login              ${TATE-USER}    ${TATE-PWD}

Open Bananapi Connection And Log In
     Set Default Configuration   timeout=10s    prompt=#
     Open Connection    ${BPI-HOST}     alias=bpi
     Login With Public Key   ${BPI-USER}   ${BPI-KEY}
