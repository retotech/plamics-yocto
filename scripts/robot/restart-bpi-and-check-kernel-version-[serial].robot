*** Settings ***
Documentation          Restart Bananapi and try to log in. Check kernel version.
...                    
Library                SSHLibrary
Suite Setup            Open Connection And Log In
Suite Teardown         Close All Connections

*** Variables ***
${TATE-HOST}           tate1
${TATE-USER}           tateusr
${TATE-PWD}            tatepw
${BPI-HOST}            tate-bananapi
${BPI-USER}            root
${BPI-PWD}             pi

*** Test Cases ***
Switch Bananapi Off and On
       Execute Command    ./tatectl.sh -f 2
       Sleep              1s           Give the relay a break 
       Execute Command    ./tatectl.sh -n 2

Boot Bananapi
    Write    ./tatectl.sh -s 2
    Set Client Configuration            prompt==>
    Read Until            Hit any key to stop autoboot
    Write Bare    x
    Read Until Prompt
    Write         dhcp 0x46000000 192.168.75.144:plamics-jenkins/base/uImage_4.3.3
    Read Until Prompt
    Write         dhcp 0x49000000 192.168.75.144:plamics-jenkins/base/sun7i-a20-bananapi.dtb
    Read Until Prompt
    Write         setenv bootargs console=ttyS0,115200 console=tty0 console=tty1 elevator=deadline rootwait mem=932M vmalloc=512M rootfstype=nfs root=/dev/nfs nfsroot=192.168.75.144:/export/targets/plamics-jenkins/base-bananian ip=dhcp 
    Read Until Prompt    
    Write         bootm 0x46000000 - 0x49000000

Log In to Bananapi
       Wait Until Keyword Succeeds  2 min   5 sec   Open Bananapi Connection

Test Uname
       ${output}=    Execute Command    uname -r
       Should Be Equal    ${output}    4.3.3-dirty


*** Keywords ***
Open Connection And Log In
     Set Default Configuration  timeout=60s
     Open Connection    ${TATE-HOST}
     Login              ${TATE-USER}    ${TATE-PWD}

Open Bananapi Connection
     Open Connection    ${BPI-HOST}
     Login              ${BPI-USER}     ${BPI-PWD}
     