# About Robot Framework
http://robotframework.org/
# [Install Robot Framework](https://github.com/robotframework/robotframework/blob/master/INSTALL.rst)
## Install pip
### on Debian/Ubuntu
```bash
sudo apt-get install python-pip
```
## Install robotframework
```bash
pip install --upgrade pip
sudo pip install setuptools
sudo pip install robotframework
```
## [Install robotframework-sshlibrary](https://github.com/robotframework/SSHLibrary)
```bash
sudo pip install robotframework-sshlibrary
```
# Running tests
```bash
robot --variable TATE-HOST:192.168.75.200 --variable BPI-HOST:192.168.75.202 freertos-ivshmem-demo.robot
```