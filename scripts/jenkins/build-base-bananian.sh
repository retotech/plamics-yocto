#!/bin/bash
EXPORT_ROOT=/export/targets/plamics-jenkins/base-bananian
EXPORT_BOOT=/export/tftpboot/plamics-jenkins/base
SRC_ROOT=/pub/plamics-jenkins/bananian-1604_rootfs
SRC_BOOT=/pub/plamics-jenkins/bananian-1604_boot

mkdir -p "$EXPORT_BOOT"
mkdir -p "$EXPORT_ROOT"

rm -rf "$EXPORT_BOOT"/*

# Make sure we don't do anything stupid here if path is not
# correctly set (might be parameterized later)
if echo $EXPORT_ROOT | grep -q "plamics-jenkins/"; then
    sudo rm -rf "$EXPORT_ROOT"/*
fi

cp "$SRC_BOOT/uImage" "$EXPORT_BOOT"/
cp "$SRC_BOOT/script.bin" "$EXPORT_BOOT"/
sudo cp -a "$SRC_ROOT"/* "$EXPORT_ROOT"/
