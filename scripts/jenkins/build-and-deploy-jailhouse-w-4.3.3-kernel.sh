#!/bin/bash
set -x
set -e
EXPORT_ROOT=/export/targets/plamics-jenkins/base-bananian
EXPORT_BOOT=/export/tftpboot/plamics-jenkins/base

if [ -d linux-stable ]; then
    cd linux-stable
    git clean -q -f -d -x
    git reset --hard
    git checkout v4.3.3
else
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
    cd linux-stable
    git checkout v4.3.3
fi

cd ..

INSTALL_DIR="$PWD/install"
mkdir -p "$INSTALL_DIR"
rm -rf "$INSTALL_DIR"/*

if [ ! -d bananian ]; then
	git clone https://github.com/Bananian/bananian.git
fi

rm -rf jailhouse
git clone https://github.com/siemens/jailhouse.git

if [ ! -d gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf ]; then
	wget -q https://releases.linaro.org/components/toolchain/binaries/latest-5/arm-linux-gnueabihf/gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf.tar.xz
	tar xf gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf.tar.xz
fi

export PATH=$PATH:"$PWD/gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf/bin"

cd linux-stable
for i in ../bananian/kernel/4.3.3/patches/*; do patch -p1 < $i; done

patch -p1 <<EOF
diff --git a/arch/arm/kernel/armksyms.c b/arch/arm/kernel/armksyms.c
index 7e45f69..dc0336e 100644
--- a/arch/arm/kernel/armksyms.c
+++ b/arch/arm/kernel/armksyms.c
@@ -20,6 +20,7 @@

 #include <asm/checksum.h>
 #include <asm/ftrace.h>
+#include <asm/virt.h>

 /*
  * libgcc functions - functions that are used internally by the
@@ -181,3 +182,7 @@ EXPORT_SYMBOL(__pv_offset);
 EXPORT_SYMBOL(arm_smccc_smc);
 EXPORT_SYMBOL(arm_smccc_hvc);
 #endif
+
+#ifdef CONFIG_ARM_VIRT_EXT
+EXPORT_SYMBOL_GPL(__boot_cpu_mode);
+#endif
EOF

# Build the linux kernel with jailhouse config
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
cp -av ../jailhouse/ci/kernel-config-banana-pi .config
make ARCH=arm olddefconfig
make -j12 uImage modules dtbs LOADADDR=40008000
make INSTALL_MOD_PATH="$INSTALL_DIR" modules_install

# Build Jailhouse
cd ../jailhouse
cp -av ci/jailhouse-config-banana-pi.h hypervisor/include/jailhouse/config.h
make KDIR=../linux-stable
make KDIR=../linux-stable DESTDIR="$INSTALL_DIR" install

# Copy jailhouse cell files and inmates
CELL_DIR="$INSTALL_DIR/usr/share/jailhouse/cells"
mkdir -p "$CELL_DIR"
cp configs/*.cell "$CELL_DIR"/
INMATE_DIR="$INSTALL_DIR/usr/share/jailhouse/inmates"
mkdir -p "$INMATE_DIR"
cp inmates/demos/arm/*.bin "$INMATE_DIR"/

# Copy all that has been installed locally
cd ../linux-stable
sudo cp -R "$INSTALL_DIR"/* "$EXPORT_ROOT"/
cp arch/arm/boot/uImage "$EXPORT_BOOT"/uImage_4.3.3
cp arch/arm/boot/dts/sun7i-a20-bananapi.dtb  "$EXPORT_BOOT"/

# Add ssh key to authorized keys if existing
SCRIPT_DIR=$(readlink -f $(dirname $0))
KEY="jenkins-key.pub"
if [ -f "$SCRIPT_DIR/$KEY" ]; then
    sudo mkdir -m 0700 -p ${EXPORT_ROOT}/root/.ssh
    cat "$SCRIPT_DIR/$KEY" | sudo tee -a ${EXPORT_ROOT}/root/.ssh/authorized_keys
    sudo chmod 600 ${EXPORT_ROOT}/root/.ssh/authorized_keys
fi
