#!/bin/bash
set -x
set -e
EXPORT_ROOT=/export/targets/plamics-jenkins/base-bananian
EXPORT_BOOT=/export/tftpboot/plamics-jenkins/base

if [ -d linux-stable ]; then
    cd linux-stable
    git clean -f -d -x
    git reset --hard
    git checkout v4.3.3
else
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
    cd linux-stable
    git checkout v4.3.3
fi

cd ..

INSTALL_DIR="$PWD/install"
mkdir -p "$INSTALL_DIR"
rm -rf "$INSTALL_DIR"/*

if [ ! -d bananian ]; then
	git clone https://github.com/Bananian/bananian.git
fi

rm -rf jailhouse
git clone https://github.com/siemens/jailhouse.git

if [ ! -d gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf ]; then
	wget -q https://releases.linaro.org/components/toolchain/binaries/latest-5/arm-linux-gnueabihf/gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf.tar.xz
	tar xf gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf.tar.xz
fi

export PATH=$PATH:"$PWD/gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabihf/bin"

cd linux-stable
for i in ../bananian/kernel/4.3.3/patches/*; do patch -p1 < $i; done

patch -p1 <<EOF
diff --git a/arch/arm/kernel/armksyms.c b/arch/arm/kernel/armksyms.c
index 7e45f69..dc0336e 100644
--- a/arch/arm/kernel/armksyms.c
+++ b/arch/arm/kernel/armksyms.c
@@ -20,6 +20,7 @@

 #include <asm/checksum.h>
 #include <asm/ftrace.h>
+#include <asm/virt.h>

 /*
  * libgcc functions - functions that are used internally by the
@@ -181,3 +182,7 @@ EXPORT_SYMBOL(__pv_offset);
 EXPORT_SYMBOL(arm_smccc_smc);
 EXPORT_SYMBOL(arm_smccc_hvc);
 #endif
+
+#ifdef CONFIG_ARM_VIRT_EXT
+EXPORT_SYMBOL_GPL(__boot_cpu_mode);
+#endif
EOF

cp -av ../jailhouse/ci/kernel-config-banana-pi .config

make ARCH=arm olddefconfig

make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j12 uImage modules dtbs LOADADDR=40008000
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH="$INSTALL_DIR" modules_install

sudo cp -R "$INSTALL_DIR"/* "$EXPORT_ROOT"/
cp arch/arm/boot/uImage "$EXPORT_BOOT"/uImage_4.3.3
cp arch/arm/boot/dts/sun7i-a20-bananapi.dtb  "$EXPORT_BOOT"/

