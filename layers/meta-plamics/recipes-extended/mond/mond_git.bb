SUMMARY = "Monitoring daemon for PLAMICS"

DEPENDS = ""

PR = "r1"
PV = "0.1+${SRCPV}"

S = "${WORKDIR}/git"

SRC_URI = " \
   	git://bitbucket.org/retotech/mond.git;protocol=https; \
   	file://mond.sh \
   	file://mond.conf \
      "
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=87291169c27b8b545e0fd44bea7fa60b"

SRCREV = "${AUTOREV}"

inherit update-rc.d
INITSCRIPT_NAME = "mond.sh"
INITSCRIPT_PARAMS = "defaults"


do_install() {
    install -d ${D}${bindir}
    install -d ${D}/etc/init.d
    install -m 755 ${S}/mond ${D}${bindir}
    install -m 755 ${WORKDIR}/mond.sh ${D}/etc/init.d/
    install -m 755 ${WORKDIR}/mond.conf ${D}/etc/
}

FILES_${PN} += "${bindir}"
