#! /bin/sh
### BEGIN INIT INFO
# Provides:             mond
# Required-Start:       jailhouse
# Required-Stop:        
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    PLAMICS Monitoring daemon
### END INIT INFO

set -e

PIDFILE="/var/run/mond.pid"
DAEMON="/usr/bin/mond"
DAEMON_ARGS=""
NAME="mond"

if [ -f /etc/${NAME}.conf ]; then
    . /etc/${NAME}.conf

    if [ ! -z "$MOND_PROCESSES" ]; then
	for p in ${MOND_PROCESSES[@]}; do
	    DAEMON_ARGS="$DAEMON_ARGS -pn $p"
	done
    fi

    if [ ! -z "$MOND_PERIOD" ]; then
	DAEMON_ARGS="$DAEMON_ARGS -r $MOND_PERIOD"
    fi

    if [ ! -z "$MOND_OUTPUT" ]; then
	DAEMON_ARGS="$DAEMON_ARGS -f $MOND_OUTPUT"
    fi

    if [ ! -z "$MOND_SOCKET" ]; then
	DAEMON_ARGS="$DAEMON_ARGS -s $MOND_SOCKET"
    fi
fi

case "$1" in
  start)
      echo -n "Starting $NAME: "
      
      start-stop-daemon --start -v --make-pidfile --pidfile $PIDFILE \
	    --background -a "/bin/bash" \
	    -- -c "exec $DAEMON $DAEMON_ARGS > /var/log/$NAME.log 2>&1"

	echo "ARGS: $DAEMON_ARGS"
	echo "done"
	;;
  stop)
	echo -n "Stopping $NAME: "
	start-stop-daemon -K -p $PIDFILE
	echo "done"
	;;
  restart)
  	$0 stop
	$0 start
	;;
  *)
	echo "Usage: mond { start | stop | restart }" >&2
	exit 1
	;;
esac

exit 0
