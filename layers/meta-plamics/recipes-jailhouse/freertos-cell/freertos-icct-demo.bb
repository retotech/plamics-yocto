SUMMARY = "FreeRTOS Inmate Used in Inter Cell Communication Demo for Jailhouse"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://freertos/License/license.txt;md5=ff3ee34806c13760712131849e183a35\
		 file://LICENSE;md5=494a529748a63071fbdf44f61db2391c"

DEPENDS = "virtual/kernel make-native"

SRC_URI = "git://bitbucket.org/retotech/plamics-freertos;protocol=https;branch=icct-demo;name=freertoscell \
	git://bitbucket.org/retotech/icc;protocol=https;branch=master;destsuffix=icc;name=icc \
        file://0001-Adapt-to-newer-toolchain.patch \
        "

SRCREV_freertoscell = "${AUTOREV}"
SRCREV_icc = "${AUTOREV}"
SRCREV_FORMAT ?= "freertoscell_icc"

PV = "0.1-git${SRCPV}"

S ="${WORKDIR}/git"

INMATES_DIR ?= "${datadir}/jailhouse/inmates"
CELLCONF_DIR ?= "${datadir}/jailhouse/configs"

do_compile() {
    LDFLAGS="" make -C freertos/Demo/icct/ ICCT_ROOT=${WORKDIR}/icc/icct
}

inherit jailhouse-cell

INMATE = "${S}/freertos/Demo/icct/freertos-icct-demo.bin"
CELLCONFIG = "${S}/jailhouse-configs/bananapi-freertos-demo.c"
CELLCONFIG_TARGET = "freertos-icct-demo.c"

