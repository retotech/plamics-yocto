SRC_URI = "git://bitbucket.org/retotech/plamics-ivshmem-guest-code.git;protocol=https;branch=plamics"

S ="${WORKDIR}/git/uio/tests/shmem"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../../../COPYING;md5=0546a27aad86c83b75ad4ee6133e9d5e"

SRCREV ="${AUTOREV}"
PV = "0.1-git${SRCPV}"

inherit cmake

utilsdir="/opt/ivshmem-utils/"

do_install() {
    install -d ${D}${utilsdir}
    install -m 0755 shmem_dump ${D}${utilsdir}
    install -m 0755 shmem_pump ${D}${utilsdir}
}

FILES_${PN} = "${utilsdir}"
