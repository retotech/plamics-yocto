SUMMARY = "Inter cell communication for Jailhouse"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=87291169c27b8b545e0fd44bea7fa60b"

SRC_URI = "git://bitbucket.org/retotech/icc;protocol=https;branch=master \
	"

SRCREV ="${AUTOREV}"

S ="${WORKDIR}/git"
PV = "0.1-git${SRCPV}"

inherit cmake

iccdir="/opt/icc/"

do_install() {
    install -d ${D}${iccdir}
    install -m 0755 linux/icctd ${D}${iccdir}
    install -m 0755 linux/icctpump ${D}${iccdir}
    install -m 0755 linux/icctdump ${D}${iccdir}
}

FILES_${PN} = "${iccdir}"
