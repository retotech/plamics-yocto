FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-git:"

SRC_URI += "file://0001-Added-virtual-PCI-support-for-ARM-32.patch \
    file://0002-Added-bare-metal-ivshmem-demo-inmate-for-Bananapi.patch \
    file://0003-Added-description-on-how-to-run-bare-metal-ivshmem-d.patch \
    file://0004-Added-debug-printouts-for-ivshmem-memory-mismatch-in.patch \
	file://plamics-bpi-root.c \
	file://jailhouse-config-bpi.h \
	"

JH_CONFIG = "${WORKDIR}/jailhouse-config-bpi.h" 

do_configure_append() {
    # Copy our own root-cell into build
    cp ${WORKDIR}/plamics-bpi-root.c ${S}/configs/
}

CELLS= "freertos-cell freertos-demo1-cell freertos-demo3-cell freertos-ivshmem-demo freertos-icct-demo"
