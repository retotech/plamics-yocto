SUMMARY = "Linux kernel module device driver for the Jailhouse ivshmem virtual PCI device"
LICENSE = "CLOSED"

inherit module

SRC_URI = "file://Makefile \
           file://uio_ivshmem.c \
          "

S = "${WORKDIR}"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
