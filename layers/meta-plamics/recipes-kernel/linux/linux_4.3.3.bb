SECTION = "kernel"
DESCRIPTION = "Mainline Linux kernel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
COMPATIBLE_MACHINE = "(sun4i|sun5i|sun7i)"

inherit kernel

require recipes-kernel/linux/linux-dtb.inc
require linux.inc

# Pull in the devicetree files into the rootfs
RDEPENDS_kernel-base += "kernel-devicetree"

KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT}"

S = "${WORKDIR}/linux-${PV}"

SRC_URI[md5sum] = "f3068333fa524ff98914cf751d0b5710"
SRC_URI[sha256sum] = "8cad4ce7d049c2ecc041b0844bd478bf85f0d3071c93e0c885a776d57cbca3cf"

SRC_URI = "https://www.kernel.org/pub/linux/kernel/v4.x/linux-${PV}.tar.xz \
        file://0001-dtb.patch \
        file://0011-bananapi_m2_dts.patch \
        file://0002-b53_switch_driver.patch \
        file://0012-bananapi_r1_dts.patch \
        file://0003-sun4i-ss_add_missing_statesize \
        file://0013-lamobo_r1_dts.patch \
        file://0010-bananapi_m1_plus_dts.patch \
        file://0020-M2-R1-M1-support.patch \
        file://defconfig \
        "

SRC_URI_append_plamics-bpim1 = " file://jailhouse-required-symbol-export.patch "

