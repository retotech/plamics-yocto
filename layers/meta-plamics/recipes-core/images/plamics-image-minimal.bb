SUMMARY = "A small image just capable of allowing a device to boot."

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"
IMAGE_INSTALL_append= " \
                      jailhouse \
                      jailhouse-bash-completion \
                      kernel-modules \
                      bash \
                      ivshmem-mod \
                      ivshmem-utils \
		      uio-utils \
		      openssh-dev \
		      icc \
		      netcat-openbsd \
		      start-stop-daemon \
                      mond \
		      "
# Packages for Webmin
IMAGE_INSTALL_append= " \
                      webmin \
                      webmin-theme-gray-theme \
                      webmin-module-net \
                      webmin-module-plamics \
                      webmin-module-syslog \
                      webmin-module-shell \
                      webmin-module-webmin \
                      webmin-module-sshd \
                      webmin-module-mediatomb \
                      webmin-module-webmincron \
                      webmin-module-system-status \
                      libnet-ssleay-perl \
                      procps \
                      "

# Packages related to bringing up a WLAN with a USB WiFi dongle
IMAGE_INSTALL_append= " \
                      usbutils \
                      wireless-tools \
                      linux-firmware \
                      rng-tools \
                      "

IMAGE_FEATURES += "ssh-server-openssh eclipse-debug package-management"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"
