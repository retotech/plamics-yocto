SUMMARY = "Hardware firmware packages not included with the Linux kernel source code"
SECTION = "base"
PR = "r1"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"


SRCREV = "a61ac5cf8374edbfe692d12f805a1b194f7fead2"

PR = "r0"

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git;branch=master"

S = "${WORKDIR}/git"

do_install () {
   install -m 0755 -d ${D}/lib/firmware
   install -m 0755 ${S}/ath9k_htc/htc_9271-1.4.0.fw ${D}/lib/firmware/htc_9271.fw
}

FILES_${PN} =  "/lib/firmware/*"