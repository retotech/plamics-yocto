#!/bin/bash

usage ()
{
  echo  "Script for controlling the Plamics Demo 3 Segway"
  echo "Usage: ${0} [-h]"
  echo " -h Help"
  echo " -s Start (load Jailhouse, cell configs etc and start inmate)"
  echo " -k Stop (stop inmate and unload everythin and unload Jailhouse)"
  exit
}

startDemo ()
{
    if ! lsmod |grep jailhouse; then
        modprobe jailhouse
        jailhouse enable /usr/share/jailhouse/cells/bananapi.cell
    fi
    jailhouse cell create /usr/share/jailhouse/cells/freertos-demo3-cell.cell
    jailhouse cell load FreeRTOS /usr/share/jailhouse/inmates/freertos-demo3.bin
    jailhouse cell start FreeRTOS
}

stopDemo ()
{
    jailhouse cell shutdown FreeRTOS
    jailhouse cell destroy FreeRTOS
}

while getopts "hsk" flag;
do
  case $flag in
    h ) usage ;;
    s ) startDemo ;;
    k ) stopDemo ;;
    * ) echo "Unknown argument" $OPTARG
    usage
    ;;
  esac
done
