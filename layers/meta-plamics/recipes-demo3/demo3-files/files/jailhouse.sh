#! /bin/sh
### BEGIN INIT INFO
# Provides:             jailhouse
# Required-Start:       $local_fs
# Required-Stop:        $local_fs
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    PLAMICS Demo3 Jailhouse bringup
### END INIT INFO

set -e

NAME="jailhouse"
JH_DIR="/usr/share/jailhouse"

ROOT_CELL="${JH_DIR}/cells/plamics-bpi-root.cell"
FREERTOS_CELL="${JH_DIR}/cells/freertos-demo3-cell.cell"
INMATE="${JH_DIR}/inmates/freertos-demo3.bin"

# icct deamon
PIDFILE="/var/run/icctd.pid"
DAEMON="/opt/icc/icctd"
DAEMON_ARGS=""

if [ -f /etc/${NAME}.conf ]; then
    . /etc/${NAME}.conf
fi



case "$1" in
  start)
      echo -n "Starting $NAME: "

      jailhouse enable ${ROOT_CELL}

      start-stop-daemon --start -v --make-pidfile --pidfile $PIDFILE \
	    --background -a "/bin/bash" \
	    -- -c "exec $DAEMON $DAEMON_ARGS >> /var/log/icctd.log 2>&1"

      jailhouse cell create ${FREERTOS_CELL}
      jailhouse cell load FreeRTOS ${INMATE}
      jailhouse cell start FreeRTOS
      echo "done"
      ;;
  stop)
      echo -n "Stopping $NAME: "
      jailhouse cell shutdown FreeRTOS
      jailhouse cell destroy FreeRTOS
      start-stop-daemon -K -p $PIDFILE
      jailhouse disable
      echo "done"
      ;;
  restart)
      $0 stop
      $0 start
      ;;
  *)
      echo "Usage: ${NAME} { start | stop | restart }" >&2
      exit 1
      ;;
esac

exit 0
