SUMMARY = "Miscellaneous files for the demo 3 system"
SECTION = "base"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

RDEPENDS_${PN} = "bash"

SRC_URI = "file://demo3Control.sh \
	file://S99udhcpd \
	file://udhcpd.conf \
	file://jailhouse.sh \
	file://jailhouse.conf \	
           "

S = "${WORKDIR}"

# Deal with the startup of jailhouse and its cells with standard type startup
# script
inherit update-rc.d
INITSCRIPT_NAME = "jailhouse.sh"
INITSCRIPT_PARAMS = "defaults"


do_install () {
    install -m 0755 -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/demo3Control.sh ${D}${bindir}/demo3Control.sh
    install -m 0755 -d ${D}/etc/rc5.d/
    install -m 0755 ${WORKDIR}/S99udhcpd ${D}/etc/rc5.d/
    install -m 644 ${WORKDIR}/udhcpd.conf ${D}/etc/
    install -m 0755 -d ${D}/etc/init.d/
    install -m 0755 ${WORKDIR}/jailhouse.sh ${D}/etc/init.d/
    install -m 0644 ${WORKDIR}/jailhouse.conf ${D}/etc/
}
