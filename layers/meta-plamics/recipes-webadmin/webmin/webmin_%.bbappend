# Disable use of som modules 
PACKAGES_remove = "${PN}-module-raid ${PN}-module-exports ${PN}-module-fdisk ${PN}-module-lvm"

do_install() {
    install -d ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/webmin
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 webmin-init ${D}${sysconfdir}/init.d/webmin

    install -d ${D}${localstatedir}
    install -d ${D}${localstatedir}/webmin

    install -d ${D}${libexecdir}/webmin
    cd ${S} || exit 1
    tar --no-same-owner --exclude='./patches' --exclude='./.pc' -cpf - . \
        | tar --no-same-owner -xpf - -C ${D}${libexecdir}/webmin

    rm -f ${D}${libexecdir}/webmin/webmin-init
    rm -f ${D}${libexecdir}/webmin/ajaxterm/ajaxterm/configure.initd.gentoo
    rm -rf ${D}${libexecdir}/webmin/patches

    # Run setup script
    export perl=perl
    export perl_runtime=${bindir}/perl
    export prefix=${D}
    export tempdir=${S}/install_tmp
    export wadir=${libexecdir}/webmin
    export config_dir=${sysconfdir}/webmin
    export var_dir=${localstatedir}/webmin
    export os_type=generic-linux
    export os_version=0
    export real_os_type="${DISTRO_NAME}"
    export real_os_version="${DISTRO_VERSION}"
    export port=10000
    export login=${WEBMIN_LOGIN}
    export password=${WEBMIN_PASSWORD}
    export ssl=1
    export atboot=1
    export no_pam=1
    mkdir -p $tempdir
    ${S}/../setup.sh

    # Ensure correct PERLLIB path
    sed -i -e 's#${D}##g' ${D}${sysconfdir}/webmin/start

    rm -rf ${D}${libexecdir}/webmin/raid
    rm -rf ${D}${libexecdir}/webmin/exports
    rm -rf ${D}${libexecdir}/webmin/fdisk
    rm -rf ${D}${libexecdir}/webmin/lvm

    # Avoid getting errors when doing automatic refresh of a page
    sed -i 's/referers_none=1/referers_none=0/' ${D}/etc/webmin/config
}
