SUMMARY = "Webmin plugin for Plamics"
SECTION = "plamics"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

#DEPENDS = "webmin"
RDEPENDS_${PN} = "webmin"

SRC_URI = "file://etc/webmin/plamics/config \
	file://usr/libexec/webmin/plamics/config \
	file://usr/libexec/webmin/plamics/index.cgi \
	file://usr/libexec/webmin/plamics/module.info \
	file://usr/libexec/webmin/plamics/plamics-lib.pl \
	file://usr/libexec/webmin/plamics/view_file.cgi \
	file://usr/libexec/webmin/plamics/handle_resolv_conf.cgi \
	file://usr/libexec/webmin/plamics/webmin_examples.cgi \
	file://usr/libexec/webmin/plamics/images/icon.gif \
	file://usr/libexec/webmin/plamics/images/smallicon.gif \
       "

do_install() {
    install -m 0755 -d ${D}/etc/webmin/plamics
    install -m 0644 ${WORKDIR}/etc/webmin/plamics/config ${D}${sysconfdir}/webmin/plamics/config

    install -m 0755 -d ${D}/usr/libexec/webmin/plamics
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/config ${D}/usr/libexec/webmin/plamics/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/index.cgi ${D}/usr/libexec/webmin/plamics/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/module.info ${D}/usr/libexec/webmin/plamics/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/plamics-lib.pl ${D}/usr/libexec/webmin/plamics/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/view_file.cgi ${D}/usr/libexec/webmin/plamics/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/handle_resolv_conf.cgi ${D}/usr/libexec/webmin/plamics/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/webmin_examples.cgi ${D}/usr/libexec/webmin/plamics/

    install -m 0755 -d ${D}/usr/libexec/webmin/plamics/images
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/images/icon.gif ${D}/usr/libexec/webmin/plamics/images/
    install -m 0644 ${WORKDIR}/usr/libexec/webmin/plamics/images/smallicon.gif ${D}/usr/libexec/webmin/plamics/images/
}

pkg_postinst_${PN} () {
#!/bin/sh -e
sed 's/^admin: /admin: plamics /' -i $D/etc/webmin/webmin.acl
}