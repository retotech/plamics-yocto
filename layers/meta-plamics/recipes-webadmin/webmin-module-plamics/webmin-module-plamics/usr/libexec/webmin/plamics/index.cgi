#!/usr/bin/perl
# Plamics related configuration and examples

require 'plamics-lib.pl';

ui_print_header(undef, $module_info{'desc'}, "", undef, 1, 1);
	

# mond
print &ui_form_start("view_file.cgi");
print &ui_hidden("title", "Monitoring daemon - mond"),"\n";               
print &ui_hidden("nofilename", 1),"\n";               
print &ui_hidden("view", 1),"\n";
print &ui_hidden("file", "/tmp/mond-util"),"\n";
print "<b>Monitoring daemon </b>\n",
    &ui_submit(mond),"\n";
print &ui_form_end();
print "<br>\n";

# mond
print &ui_form_start("webmin_examples.cgi");
print "<b>Webmin examples</b>\n",
    &ui_submit(Go),"\n";
print &ui_form_end();
print "<br>\n";


ui_print_footer('/', $text{'index'});
