#!/usr/bin/perl
# Plamics related configuration and examples

require 'plamics-lib.pl';

ui_print_header(undef, $module_info{'desc'}, "", undef, 1, 1);

# View a selectable log file
print &ui_form_start("view_file.cgi");
print &ui_hidden("view", 1),"\n";
print "<b>View a complete file </b>\n",
    &ui_textbox("file", undef, 50),"\n",
    &file_chooser_button("file", 0, 1),"\n",
    &ui_submit(View),"\n";
print &ui_form_end();
print "<br>\n";


# View a defined log file
print &ui_form_start("view_file.cgi");
print &ui_hidden("view", 1),"\n";
print &ui_hidden("file", "/var/log/messages"),"\n";
print "<b>View the /var/log/messages file</b>\n",
    &ui_submit(View),"\n";
print &ui_form_end();
print "<br>\n";


# View last lines of a file
print &ui_form_start("view_file.cgi");
print &ui_hidden("viewPartOfFile", 1),"\n";
print &ui_hidden("tail", 1),"\n";
print "<b>View a last ",
    &ui_textbox("partlines", 20, 3),"\n",
    "lines from a file</b>\n",
    &ui_textbox("file", undef, 50),"\n",
    &file_chooser_button("file", 0, 1),"\n",
    &ui_submit(View),"\n";
print &ui_form_end();
print "<br>\n";


@files = ("/var/log/messages", "/proc/cmdline", "/proc/interrupts");

print &ui_form_start("view_file.cgi");
print &ui_hidden("viewPartOfFile", 1),"\n";
print &ui_table_start("View nr of lines from a file", undef, 2);
print &ui_table_row("Tail?",
		    &ui_yesno_radio("tail", 1));

print &ui_table_row("Nr of lines to show", &ui_textbox("partlines", 20, 3));
print &ui_table_row("File", &ui_select("file", undef, \@files));
print &ui_table_row(&ui_submit(View),"\n");
print &ui_table_end();
print &ui_form_end();
print "<br>\n";                                                             


# Remove /etc/resolv.conf           
print &ui_form_start("handle_resolv_conf.cgi");
print &ui_hidden("remove", 1),"\n";                  
print "<b>Remove /etc/resolv.conf </b>\n",  
    &ui_submit(OK),"\n";                         
print &ui_form_end();                              

# Restore /etc/resolv.conf                                        
print &ui_form_start("handle_resolv_conf.cgi");                             
print &ui_hidden("restore", 1),"\n";                                         
print "<b>Restore /etc/resolv.conf </b>\n",                                  
    &ui_submit(OK),"\n";                                                    
print &ui_form_end();                                                       
  
print "<br>\n";                                    
                 

ui_print_footer('/', $text{'index'});
