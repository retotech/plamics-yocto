#!/usr/bin/perl

require './plamics-lib.pl';
&ReadParse();
&foreign_require("proc", "proc-lib.pl");
#$conf = &get_config();


&ui_print_header("<tt>".&html_escape($file || $cmd)."</tt>",
			 "/etc/resolv.conf", "");

if ($in{'remove'}) {
        print "<pre>\n";                                                                     
	$got = &proc::safe_process_exec(                                                 
                "mv /etc/resolv.conf /etc/resolv.conf_orig",           
                0, 0, STDOUT, undef, 1, 0, undef, 1);    
        print "/etc/resolv.conf removed</pre>\n";                                            
}


if ($in{'restore'}) {                                                
        print "<pre>\n";                                            
        $got = &proc::safe_process_exec(                             
                "mv /etc/resolv.conf_orig /etc/resolv.conf",       
                0, 0, STDOUT, undef, 1, 0, undef, 1);                
        print "/etc/resolv.conf restored</pre>\n";                                            
}                                                                                            
 


&ui_print_footer('/', $text{'index'});
