#!/usr/bin/perl
# view_file.cgi
# View a file

require './plamics-lib.pl';
&ReadParse();
&foreign_require("proc", "proc-lib.pl");
#$conf = &get_config();


$refreshRate = 1;
# Refresh page every refreshRate second
#
# Note: If an error appears with "Warning! Webmin has detected that the program was linked to from an unknown URL, which appears to be outside the Webmin server. This may be an attempt to trick your server into executing a dangerous command."
# You can disable the Trusted Referrers check by going into Webmin -> Webmin -> Webmin Configuration -> Trusted Referrers, and changing the check from "Yes" to "No".
print "Refresh: $refreshRate\r\n";

if ($in{'title'})
{
	&ui_print_header("<tt>".&html_escape($file || $cmd)."</tt>",
			 $in{'title'}, "");
}
else
{
        &ui_print_header("<tt>".&html_escape($file || $cmd)."</tt>",                                    
                         "View file", "");        
}

if ($in{'view'}) {
    if ($in{'file'}) {
	print "<pre>\n\n";
        if (!$in{'nofilename'})                                                    
	{
        	print "$in{'file'}:\n\n";
	}
	open_readfile(fh, $in{'file'}) || return 0;
	
	while ($row = <fh>) {
	    chomp $row;
	    print "$row\n";
	}
	
	close($fh);
	print "</pre>\n";
    }
}

if ($in{'viewPartOfFile'}) 
{
    if ($in{'file'}) 
    {
	print "<pre>\n";
	if ($in{'tail'}) 
	{
	    print "Last $in{'partlines'} lines of $in{'file'}:\n\n";
	    $got = &proc::safe_process_exec(
		"cat $in{'file'}| tail -$in{'partlines'}",
		0, 0, STDOUT, undef, 1, 0, undef, 1);
	}
	else
	{
	    print "First $in{'partlines'} lines of $in{'file'}:\n\n";
	    $got = &proc::safe_process_exec(
		"cat $in{'file'}| head -n $in{'partlines'}",
		0, 0, STDOUT, undef, 1, 0, undef, 1);
	}
	    while ($row = <got>) {
		chomp $row;
		print "$row\n";
	    }
	
	print "</pre>\n";
    }
    
}

#print "<pre>\n";
#print "This page will be refreshed every $refreshRate second\r\n";
#print "</pre>\n";

&ui_print_footer('/', $text{'index'});
