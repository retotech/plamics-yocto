# PLAMICS Yocto build environment

## Preparing

Imported OE layers are in `./layers/` as git submodules.

PLAMICS local files are for now in this git repo in `./layers/meta-plamics/`.
These files can later on be moved to a separate git repo.

To set up for a build:
```bash
TEMPLATECONF=../meta-plamics/conf source layers/poky/oe-init-build-env build
```

Clonig this repo:
```bash
git clone --recursive ...../plamics-yocto.git
```

Updating this repo:
```bash
git pull
git submodule update --init --recursive
```

## Building

Build image:
```bash
time MACHINE=qemuarm bitbake core-image-minimal
time MACHINE=bananapi bitbake core-image-minimal
time MACHINE=plamics-bpim1 bitbake core-image-minimal
time MACHINE=plamics-bpim1 bitbake plamics-image-minimal
time MACHINE=plamics-bpim1 bitbake plamics-image-demo3
```

Build SDK:
```bash
bitbake -c populate_sdk plamics-image-minimal
```

## Running image on BPi-M1 target

### Preparing

Write image to SD-card:
Insert an SD-card to which the built image may be written to into the machine on which the image was built. Assuming that the SD-card appears as `/dev/sdc` (check with e.g. `lsblk`):
```bash
sudo umount /dev/sdc*
sudo dd if=tmp/deploy/images/plamics-bpim1/plamics-image-minimal-plamics-bpim1.sunxi-sdimg of=/dev/sdc
sync
```

Eject the SD-card from the build machine and insert it into the BPi-M1 and reboot. The boot console is available on the serial-port UART0 of the BPi-M1.

Connect a serial port to the UART0 of the BPi-M1 using e.g. `minicom -D /dev/ttyUSB0`.

Connect a serial-port to the UART7 of the BPi-M1 using e.g. `minicom -D /dev/ttyUSB1`. This serial-port is used by demo inmates for serial/terminal I/O.

### Starting up the icct-demo
Once the boot-sequence has completed and the login shell appears in the terminal connected to UART0, log in on the BPi-M1. User name is `root`, no password.
```bash
login: root
root@plamimcs-bpmi1:~#
```

Load the Jailhouse LKM:
``` bash
modprobe jailhouse
```

Enable the Jailhouse root cell:
```bash
jailhouse enable /usr/share/jailhouse/cells/plamics-bpi-root.cell
```

Start the ICCT daemon:
```bash
/opt/icc/icctd &
icctd_job_pid=$!
```
This step shall be performed prior to starting any inmates using the IVSHMEM virtual PCI device for communicating with the root-cell, e.g. the FreeRTOS ICCT demo.

Create the FreeRTOS ICCT demo cell:
```bash
jailhouse cell create /usr/share/jailhouse/cells/freertos-icct-demo.cell
```

Load the FreeRTOS ICCT demo inmate into the FreeRTOS ICCT demo cell:
```bash
jailhouse cell load FreeRTOS /usr/share/jailhouse/inmates/freertos-icct-demo.bin
```

Start the FreeRTOS ICCT demo inmate:
```bash
jailhouse cell start FreeRTOS
```

### Running the icct-demo

To send messages from the Linux root-cell to the FreeRTOS icct-demo inmate, open a connection to icctd using the Unix domain socket provided by the socket file `UDS_icctd`.
This can be done by running:
```bash
echo -en 'Hello from root-cell!\x0' | nc -U UDS_icctd
```
or:
```bash
/opt/icc/icctpump
```
### Shutting down the icct-demo

Shut down FreeRTOS ICCT demo inmate:
```bash
jailhouse cell shutdown FreeRTOS
```

Destroy FreeRTOS ICCT demo cell:
```bash
jailhouse cell destroy FreeRTOS
```

Stop the ICCT daemon in the root-cell:
```bash
kill $icctd_job_pid
```

Disable the Jailhouse root cell:
```bash
jailhouse disable
```

Unload the Jailhouse LKM:
```bash
rmmod jailhouse
```

## Developing Linux applications using SDK

Having built the SDK, install it like so:
```bash
cd tmp/deploy/sdk/
./poky-glibc-x86_64-plamics-image-minimal-cortexa7hf-neon-vfpv4-toolchain-2.2.1.sh

```
Now, source the environment setup script (provided that the default target directory for SDK, `/opt/poky/2.2.1`, was selected in the previous step):
```bash
. /opt/poky/2.2.1/environment-setup-cortexa7hf-neon-vfpv4-poky-linux-gnueabi
```

It shall now be possible to build applications using the SDK, e.g.:
```bash
cd ~/icc
mkdir build
cd build
cmake ..
```

Once the application is built, it is now possible to download it to the target, e.g. like such:
```bash
scp user@host:~/icc/build/linux/icctd /opt/icc/icctd
```

## Developing FreeRTOS inmates using SDK

Either use the SDK, or proceed as described in ../freertos/Demo/readme.md.

Once the inmate is built, it is now possible to download it to the target, e.g. like such:
```bash
scp user@host:~/freertos-cell/freertos/Demo/icct/freertos-icct-demo.bin /usr/share/jailhouse/inmates/freertos-icct-demo.bin
```
