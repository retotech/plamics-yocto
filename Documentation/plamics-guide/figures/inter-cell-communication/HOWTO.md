The files in this directory with `.uml`-suffix are figures described in PlantUML syntax.

# Install PlantUML
http://plantuml.com/starting

# Running
`java -jar <plantuml.jar> <name>.uml` or simply: `java -jar <plantuml.jar>` which starts a file browser to use for selecting which figure to display.