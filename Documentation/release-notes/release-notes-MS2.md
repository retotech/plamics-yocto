Release notes for PLAMICS Milestone 2 (MS2)
===========================================

This document describes versions of artifacts produced and delivered as part of
MS2 of the PLAMICS project.

It also describes how we have met our definition of done (DoD), and what issues,
if any, that remain and have been postponed.


Status of PLAMICS Wiki
----------------------

Updated.


Status of project documents
---------------------------

plamics-guide (0.2) - We have an updated version of plamics-guide
which will later become the design description for the project. The document
currently includes build instructions, instruction on how to use the Eclipse
development environment for Linux and a new chapter on Inter-Cell Communication.

This document has been published here: http://pripps.retotech.se/plamics/MS2/plamics-guide/plamics-guide.html

mond-guide (1.0) - We have a first version of mond-guide which contains the Monitoring Requirements Specification and later shall contain the Monitoring Design Specification and User's Guide.

This document has been published here: http://pripps.retotech.se/plamics/MS2/mond-guide/mond-guide.html

Git repositories and tags
-------------------------

We currently use the following git repositories for dealing with documents and
code in the PLAMICS project:

| Repository         | Short Description                        | MS2 tags | Comment
|--------------------|------------------------------------------|----------|----
| plamics-yocto.git  | Top level repository for Yocto build env | v0.2 MS2 | Pulls in poky etc. via submodules 
| jailhouse.git      | Work repo for Jailhouse                  | plamics-v0.2 MS2 | Follows git://github.com/siemens/jailhouse.git
| freertos-cell.git  | Work repo for freertos-cell.             | plamics-v0.2 MS2 | Follows git://github.com/siemens/freertos-cell.git
| jenkins-configs.git| Config files for Jenkins CI environment  | v0.2     | Only used for backup
| base.git           | Contains project documentation.          | plamics-v0.2 |

Both the jailhouse and freertos-cell repositories have a master branch that
follows the upstream master. The plamics branch is used for the PLAMICS
project's stable changes. The demo1 branch in the freertos-cell repository
includes an inmate implementation that makes up the demo.

Remaining issues part of MS2
----------------------------
* #549: Implement and test first basic communication
* #550: Design specification for communication
* #551: Communication implementation
* #553: Monitoring requirements gathering
