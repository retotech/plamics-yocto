Release notes for PLAMICS Milestone 1 (MS1)
===========================================

This document describes versions of artifacts produced and delivered as part of
MS1 of the PLAMICS project.

It also describes how we have met our definition of done (DoD), and what issues,
if any, that remain and have been postponed.


Status of PLAMICS Wiki
----------------------

Updated.


Status of project documents
---------------------------

plamics-guide (V0.1) - We have produced a first version of plamics-guide
which will later become the design description for the project. The document
currently includes build instructions and instruction on how to use the Eclipse
development environment for Linux.

This document has been published here: http://pripps.retotech.se/plamics/MS1/plamics-guide/plamics-guide.html


Git repositories and tags
-------------------------

We currently use the following git repositories for dealing with documents and
code in the PLAMICS project:

| Repository         | Short Description                        | MS1 tags | Comment
|--------------------|------------------------------------------|----------|----
| plamics-yocto.git  | Top level repository for Yocto build env | v0.1 MS1 | Pulls in poky etc. via submodules 
| jailhouse.git      | Work repo for Jailhouse                  | plamics-v0.1 MS1 | Follows git://github.com/siemens/jailhouse.git
| freertos-cell.git  | Work repo for freertos-cell.             | plamics-v0.1 MS1 D1 | Follows git://github.com/siemens/freertos-cell.git
| jenkins-configs.git| Config files for Jenkins CI environment  | v0.1     | Only used for backup
| base.git           | Contains the documentation.              | plamics-v0.1 | The documentation will be moved to plamics-yocto.git

Both the jailhouse and freertos-cell repositories have a master branch that
follows the upstream master. The plamics branch is used for the PLAMICS
project's stable changes. The demo1 branch in the freertos-cell repository
includes an inmate implementation that makes up the demo.


Remaining issues part of MS1
----------------------------

* #602: Fill in information on how to connect and use the D1 demo.
The demonstrator works today but instructions on how to use it
are needed in the documentation. (https://badger.retotech.se/issues/602)
**Rationale for postponing:** The D1 demonstrator is functioning which was the
ultimate goal of the milestone.The documentation of its use is okay to include
at a later stage.

* #534: Create all project documents.
We intended to create all project documents in WP1 (being empty) just to be
able to track their creation easier later on. Only one document has been
created, plamics-guide. (https://badger.retotech.se/issues/534)
**Rationale for postponing:** Having a bunch of empty documents is not really
important - just good to make sure we don't miss any in the end. We'll do that
early on in the coming phase.


