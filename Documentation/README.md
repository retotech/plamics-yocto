Documentation
=============

This is the directory that contains the Plamics Project documentation.
For a complete description of the documents produced in the project, check
https://badger.retotech.se/projects/plamics/wiki/Dokumenthantering

Organization
============

Docbook documents
-----------------
These are XML-based documents.
Folders exist for individual documents as follows:

* plamics-guide    - The PLAMICS platform design description (D7,D10)
* mond-guide       - The PLAMICS monitor design description (D4,D5)

Each of these folders is self-contained regarding content and figures.
Validation, build and publishing of these documents is controlled by the
Makefile described below.

Project reports
---------------
The project reports are located in the reports/ directory.
These are reports that the project administrators at CPSE-labs require.
They are written in Word format and based on templates from the
CPSE-labs project.

* Interim progress report (D8)
* Final report (D9)

Release notes
-------------
When a milestone is passed (MS1-MS4) we create a release notes document that
describes how our definition of done has been achieved for the milestone.

Release notes are written in Markdown format and are loated in the
release-notes/ directory.

Makefile
========

The Makefile processes manual directories to create HTML, PDF,
tarballs, etc.  Details on how the Makefile work are documented
inside the Makefile.  See that file for more information.

To build a document, you run the make command and pass it the name
of the folder containing the document's contents.
For example, the following command run from the documentation directory
creates an HTML version of the Plamics guide.
The DOC variable specifies the manual you are making:

     $ make DOC=plamics-guide

plamics.ent
===========
This file defines variables used for documentation production.  The variables
are used to define release pathnames, URLs for the published manuals, etc.

template
========
Contains various templates and fonts.

tools
=====
Contains a tool to convert the DocBook files to PDF format.

meta
====
Contains .rnc file for Emacs XML validation.


External tool required
======================
Some external tools might need to be installed on your computer such as:
sudo apt-get install xsltproc fop