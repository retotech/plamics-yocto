# Mond design chapter

## Overview

Mond will be configurable to handle startup and shutdown of all the 
applications/processes that it is assigned to manage, together with the system
as a whole. Mond will trigger on "events" occuring in the system and take well 
defined actions based on these events and the governing configuration. 
These actions may be to, for example, trigger an alarm, restart or shutdown a
process, or restart the whole system.

The user defines a number of system or process states that will, when reached,
trigger events that cause mond to take action. The user also defines
all actions mond takes on these events. Two base events, that always need 
appropriate actions are **startup** and **shutdown**.

Mond's behavior will be configurable using a configuration file read at 
startup, but it will also be reconfigurable in runtime, without requiring a 
system or mond restart. Performing a (re-)configuration is transaction based
and performed through a configuration interface. Configuration files will be
written with json syntax.

### Configuration

The configuration covers four different aspects of a system.

#### Startup

Defines what applications should be started in different modes of the system,
and in what order these applications should be started. System modes are also
listed and defined here.

#### Shutdown

Defines how the system is taken down to a shutdown mode. In what order they are
shut down, how they are shut down, and what is done if an application refuses
to stop. 

#### System states

The definition of system states is the basis for all actions performed by mond.
A state may be a process taking up too much CPU during a too long interval,
or the used system memory increasing over a given watermark. A state that will
be monitored in the Plamics platform is the communication link between the
Linux and FreeRTOS cells. A system state to monitor may in that case be
the appearance of regular heartbeats from the other cell. It may also be
making sure that the commuication threads do not take up too much of the
processor time.

#### Actions

The mond configuration defines Actions to be carried out when a system state
has been reached. An action may for instance be to kill or restart an
application, or to perform a cold or warm restart of the complete system.

### Mond's functional blocks

![Alt text](figures/mond-design-block-dia.png)

#### mondadm

This is the command line interface for mond. `mondadm` is used to retrieve
information from the mond daemon in runtime, as well as controlling it's
execution. `mondadm` can also be used to trigger a reconfiguration given
a new configuration file or using command line parameters.

#### mond

This is mond's main thread. It performs monitoring based on the loaded
configuration and takes actions when needed. It uses a number of monitoring
plugins (such as `cpu-mon`, `mem-mon`, etc.) that are capable of providing mond
with monitoring data for different aspects of the system.

The `mond` thread normally runs on low (background) priority such that it will
not interfere much with the other processes of the system. As such, it can
also be configured to serve a system watchdog (`wd`). If mond is not able to
kick the watchdog regularly, the system is reset by the watchdog. This ensures
that mond is not starved completely.

`mond` comes in two versions, a master and a slave. The mond-master has the
main responsibility of the system and uses mond-slave (if it is available)
for support.

The `mond-slave` is used when mond is spread out over more than one distinct
and hetereogenous nodes, such as in an asymmetric multi-processor system (AMP).
The `mond-slave` and `mond-master` are in contact through remote monitoring
plugins, `rem-mon`, supporting the mond process/thread on both sides of the
system.

#### mondh

`mondh` is `mond`'s high priority sibling. `mondh` runs with a high priority,
ensuring that it will be allowed to execute also on the most heavily loaded
CPUs. `mondh` is primarily responsible to monitor that no process "hogs" the
processor, taking up too much CPU-time and starving the other processes of the
system. If that is the case, `mondh` will be able to take action,
such as restarting the failing process, or moving the system into a different
execution mode.

#### cpu-mon, mem-mon, io-mon, proc-mon, rem-mon

The `cpu-mon`, `mem-mon`, `io-mon`, `proc-mon` and `rem-mon` are all monitor
plugins that are used by mond and mondh to monitor different aspects of the
the system governed by mond.

